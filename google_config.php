<?php
 
// load GCS library
require_once 'vendor/autoload.php';
 
use Google\Cloud\Storage\StorageClient;
 
// Please use your own private key (JSON file content) which was downloaded in step 3 and copy it here
// your private key JSON structure should be similar like dummy value below.
// WARNING: this is only for QUICK TESTING to verify whether private key is valid (working) or not.  
// NOTE: to create private key JSON file: https://console.cloud.google.com/apis/credentials  
$privateKeyFileContent = '
{
  "type": "service_account",
  "project_id": "testing-qr-code-8083f",
  "private_key_id": "e249066cb9381ad21f60174ac42578260160ba81",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCP0Haw54vsKVsP\nqtBOcsWy+2d6DM+ZaiDrEWUTPmbciv9ED+UdVnzSj15hwqn3rJxVOzGt44zPknOR\nX9lQRJ0FnFLe9/QyfIBdDX7ifPUjzNOtJZiemykzhY7/s+0WbGpuKpofVDWKLWTw\nw5VGUDdxt31YolUZx/nvR4gLCZR1gd31+4bmeGzfPaFu91TpotMPqArkukKmeqKA\n4R6NY7y8WrD8FWcWH4u7ZTsirJz4IIGQlk7lAXsp9+vr4zesc0gN2tXU3Q0juGO6\nYxLtWDFXHLWASq+Pqhl83UrEFTp+9LU4/r+ZNJnEb2Y4+XgWKzn2AY/6KahjA2fD\nnhAruOTjAgMBAAECggEAQiCKOuy1F3mqn+kckD3Q7uwPl/zFBldY3I2qiq8yT+1v\n06V1ZciLa6w1q0D9s19AoSYvQTQ03osKLcnDXFe4VcicCF89/2rlASR/9/eaJoR7\nvo1fgGp7BvA+IRBkD1/5jj9zj71CQw99bwGXpXWkENtcvQEjnU+o+l/LS7vzchX8\n1mrtv7oTXc99DkjtvaZ9880iZ7xeH6hjk1AgtTk02AkHHDQjAnR3vC3Bl7SsfxId\n8VuBlkYOTpQTam5GutDpclfsCgRmDIYUqM/NwNkYp5aXcdcU1l+SayeIhzAMkFXF\nlp21W+/vs2VmnA27lH8xU8uhQqhAhraaysCTHOSOaQKBgQDH0VXaOljDs9ddwLqI\n5RxccNxJw0dT7wvmPxU/eUfv2YROflwHxnu2I7Ca/B+q4GlYzHv091NaDPKVIqVh\nl3qY/QsR8wsH1/CGXHgurx31SCz7DcreKAJEQarkUl/aOb8UESO3a36xFaQiDN09\ntFFDzcU/AKLvar1iOuLNpo7x6QKBgQC4QBA1LUlxoxcZKwdOcLXRuZ5xeeC+RJbn\nNiXNSVlaIVehUwDwTqNCYaisumZKY+DuJRFvnUrvFW5s3X0DmCu5raXO4kvxo4Bk\nhMZt7sf9kesV21A8DxFkc19B/TvYXUg8GOFU1Yx0h1mHE7aYEwjRjjVq7fONt3Dh\nTZfMLoG06wKBgQC/h92WL6VT+XeviwSCIbKcvSW6oq8lOhaa3c7dJ8CXnFUg9Cf8\n3/lHLlCjTpaAMVlpX6BVwBiqUYzY/PuvXwaJzMumqePh4c/QyqP8g6jgqw1Km/7g\nEVOS36sR7g+o2v9YGdG1iBlJXugUJ2aONeBLcNrnuBF2Ekdz9+PcohGUuQKBgQCh\n4I8c2VP8hghmMVqy/O2RfF6LXkQphoN6UiV2LipVuJOQBif9+WkQilKypvX2B7h4\nIpMmF/DdC1xAQ3xxawGHmdviXwnQHBFSvyDDNxX4X3pGcrAUhDyIn6CijoVEu/gZ\n7NYUPaawPrZWqegzxg1qZ2HhN3xTF0cSe6SRXI7SzwKBgH7Od4j9LIih7GX1HHnf\nJ0AZ3Bbn8YuQQWe6yU+4eEVf1LnsuU/5ROGBCo1+VuF/FPaMsE7EuNkaW83Tem6y\n3ALY1PhPWxs6FhHGXJsNp/6tVN6s400dUmAa1OVRi+uHlnDixXDOnk70N7SREOLv\nMXWgvA2wudRlvvhjGiv9IfWu\n-----END PRIVATE KEY-----\n",
  "client_email": "upload@testing-qr-code-8083f.iam.gserviceaccount.com",
  "client_id": "111397195792935640594",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/upload%40testing-qr-code-8083f.iam.gserviceaccount.com"
}';
 
/*
 * NOTE: if the server is a shared hosting by third party company then private key should not be stored as a file,
 * may be better to encrypt the private key value then store the 'encrypted private key' value as string in database,
 * so every time before use the private key we can get a user-input (from UI) to get password to decrypt it.
 */
 
function uploadFile($bucketName, $fileContent, $cloudPath) {
    $privateKeyFileContent = $GLOBALS['privateKeyFileContent'];
    // connect to Google Cloud Storage using private key as authentication
    try {
        $storage = new StorageClient([
            'keyFile' => json_decode($privateKeyFileContent, true)
        ]);
    } catch (Exception $e) {
        // maybe invalid private key ?
        print $e;
        return false;
    }
 
    // set which bucket to work in
    $bucket = $storage->bucket($bucketName);
 
    // upload/replace file 
    $storageObject = $bucket->upload(
            $fileContent,
            ['name' => $cloudPath]
            // if $cloudPath is existed then will be overwrite without confirmation
            // NOTE: 
            // a. do not put prefix '/', '/' is a separate folder name  !!
            // b. private key MUST have 'storage.objects.delete' permission if want to replace file !
    );
 
    // is it succeed ?
    return $storageObject != null;
}
 
function getFileInfo($bucketName, $cloudPath) {
    $privateKeyFileContent = $GLOBALS['privateKeyFileContent'];
    // connect to Google Cloud Storage using private key as authentication
    try {
        $storage = new StorageClient([
            'keyFile' => json_decode($privateKeyFileContent, true)
        ]);
    } catch (Exception $e) {
        // maybe invalid private key ?
        print $e;
        return false;
    }
 
    // set which bucket to work in
    $bucket = $storage->bucket($bucketName);
    $object = $bucket->object($cloudPath);
    return $object->info();
}
//this (listFiles) method not used in this example but you may use according to your need 
function listFiles($bucket, $directory = null) {
 
    if ($directory == null) {
        // list all files
        $objects = $bucket->objects();
    } else {
        // list all files within a directory (sub-directory)
        $options = array('prefix' => $directory);
        $objects = $bucket->objects($options);
    }
 
    foreach ($objects as $object) {
        print $object->name() . PHP_EOL;
        // NOTE: if $object->name() ends with '/' then it is a 'folder'
    }
}