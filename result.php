<?php
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="main2.css">


<meta property="og:image" itemprop="image" content="<?=$img;?>">
<script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js"></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script type="text/javascript">
jQuery(function($) {
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

$(".w3-button").on("click", function(){
            var text = $(this).attr("data-message");
            var phone = $(this).attr("data-number");
            var message = encodeURIComponent(text);

    if( isMobile.any() ) {
            //mobile device
            var whatsapp_API_url = "whatsapp://send";
            $(this).attr( 'href', whatsapp_API_url+'?phone=' + phone + '&text=' + message );
        } else {
            //desktop
            var whatsapp_API_url = "https://web.whatsapp.com/send";
            $(this).attr( 'href', whatsapp_API_url+'?phone=' + phone + '&text=' + message );
        }

});
</script>
<h1 class="title">GET YOUR QR CODE</h1>
</head>
<body>
<div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading1">
                <div class="card-body">
 

      <img src="QR1.png" style="width:20%;">
        <div class="drop">
        <div class="style1"><a class="w3-button w3-account w3-clearfix style1a" style="cursor: pointer" data-number="" data-message="<?=$img;?>" target="_blank"><i  class="fab fa-whatsapp fa-7x"></i> </a> <h1 class="font1">send to whatsapp</h1></div>
        <div class="style2">
        <a href="<?=$img;?>" download><img src="images/technology.png" class="style2b"></a><h1 class="font2">Save to Device</h1></div>
        <div class="style3">
        <a href="mailto:?subject=Here is your QR Code&amp;body=Check out this link  <?=$img;?>"><img src="images/interface.png"class='style3c'></a><h1 class="font3">Send to Email</h1>
        </div>
    <a href="index.php"class="btn btn--radius btn--green">Generate another code</a>
    <h1></h1>
  </div></div></div></div>
<h1 class="some">Powered By KYC Technologies<br>Botswana</br></h1>
</body>
</html>