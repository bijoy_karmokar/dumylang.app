<?php
session_start();
if(isset($_POST['generate_seesion']))
{
	//generate QR Code
	define('IMAGE_WIDTH',500);
	define('IMAGE_HEIGHT',500);
	date_default_timezone_set("Australia/Sydney");
	$curenntdate=date("YmdHis");
	
	$data  = array(
		'id' => $_POST['text1'],
		'firstname' => $_POST['Fname'],
		'lastname' => $_POST['Lname'],
		'birthday' => $_POST['birthday'],
		'country' => $_POST['country'],
		'phone' => $_POST['Phone'],
		'address' => $_POST['address'],
	);
	
	$url = "https://testing-qr-code-8083f.firebaseio.com/users/".strtolower($_POST['Fname'])."/".$_POST['text1'].".json";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));

	$server_output = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close ($ch);
	
	if ($httpcode == 200) 
	{
		include "tmpcode/qrlib.php";
		include 'google_config.php';
		
		QRcode::png($_POST['text1'], './tmp/aaaaaaa.png', "L", 19, 19);
		$bucketName = "testing-qr-code-8083f";
        // get local file for upload testing
        $fileContent = fopen('./tmp/aaaaaaa.png','r');
        // NOTE: if 'folder' or 'tree' is not exist then it will be automatically created !
        $cloudPath = '/qrimage/aaaaaaa.png';
 
        $isSucceed = uploadFile($bucketName, $fileContent, $cloudPath);
 
        if ($isSucceed == true) {
            $response['msg'] = 'SUCCESS: to upload ' . $cloudPath . PHP_EOL;
            // TEST: get object detail (filesize, contentType, updated [date], etc.)
            $response['data'] = getFileInfo($bucketName, $cloudPath);
        } else {
            $response['code'] = "201";
            $response['msg'] = 'FAILED: to upload ' . $cloudPath . PHP_EOL;
        }
	}
	//QRcode::png($ids, $filename, "L", 19, 19);
	//$_SESSION['filename']=$filename;
	//header("Location:result.php");
}
else
{
	header("location:index.php");
}